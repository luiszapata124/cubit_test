import { Home } from "./pages/Home"
import { ListUsers } from "./pages/ListUsers"
import { Users } from "./pages/User"
import { Questions } from "./pages/Questions"
import { Menu } from "./pages/Menu"

import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link 
} from 'react-router-dom'

export function App() {
    return ( 
    
    <Router>
        <Link to="/home">
            <button type="button" className = "menuButton">
                Inicio
            </button>
        </Link>
        <Link to="/listusers">
            <button type="button" className = "menuButton">
            Usuarios
            </button></Link>
        <Link to="/questions">
            <button type="button" className = "menuButton">
                Preguntas
            </button></Link>
        <Switch>
        <Route exact path="/">
            <Menu />
            </Route>
            <Route path="/home">
                <Home />
            </Route>
            <Route path="/listusers/">
                <ListUsers />
            </Route>
            <Route path="/users/:id">
                <Users />
            </Route>
            <Route path="/questions">
                <Questions />
            </Route>

        </Switch>

    </Router>
    
    )
}


             


