import React from "react";
import { Fragment } from "react";
import { Question } from "../components/Question";
import { AnswerButton } from "../components/AnswerButton";


export const Questions = () =>{
    return (
        <Fragment>
            <header>
                <h1 className='Title'>Preguntas</h1>
            </header>
            <main>
                <div>
                    <ul>
                    <Question question='¿Qué es una prueba unitaria, cuándo se aplica y que nos permite?'></Question>
                    <AnswerButton answer='Una prueba unitaria es una forma de comprobar que un único bloque 
                    de código funciona de manera correcta y segura antes de ser añadido y unido al proyecto principal.
                        Se realizan durante la etapa de desarrollo de un proyecto ya sea por los propios desarrolladores o
                        por el equipo de QA.
                        Nos permite probar el código en cualquiera de los casos que se pueden presentar y asegurar que no haya fallas
                        en el futuro.
                    '/>

                    <Question question='¿Cuáles son las ventajas de la programación funcional?'></Question>
                    <AnswerButton answer='En principio, la programación funcional permite una sintaxis mucho más limpia, entendible y ordenada.
                        Ya que, por ejemplo, trata las funciones como valores y pasa las misma a otras funciones como parámetros 
                        y el valor de retorno dado por tales funciones es el mismo que el resultado producido por ellas,
                        por lo que falicita la legibilidad del código.
                        Además, nos permite usar la evaluación diferida, lo que significa que el valor se evalúa y almacena solo cuando es necesario.
                    '/>

                    <Question question='Pros y Contras del uso de GitFlow vs Trunk.'></Question>
                    <AnswerButton answer='Entre los Pros del uso de Gitflow tenemos que aumenta el uso del pull request y
                    y además permite un controle estricto de los cambios realizados al código. Es importante destacar que 
                    es simple de entender y aplicar. Entre sus contras, está el que al necesitar tanto control genera dependencia a una o 
                    varias personas encargadas de su revisión, además, al permitir las branches de larga vida se pueden generar dos líneas 
                    de tiempo en las branches si no se controlan de manera adecuada.
                    Respecto a Trunk-based development entre sus pros está que los release se pueden generar en el momento deseado, ya que el código
                    siempre está disponible para ello, además, el equipo de desarrollo siempre puede trabajar con el código más reciente. Entre sus 
                    principales contras está el que los desarrolladores son directamente responsables por subir código de calidad, por lo que
                    puede generar conflictos futuros.'/>

                    <Question question='¿Cuáles son las ventajas de utilizar arquitecturas sin servidor?'></Question>
                    <AnswerButton answer='La primera de los principales ventajas de no usar un servidor es la diminución en los costos,
                        ya que no requiere un perfil específico para manejar dicho servidor. Como segunda ventaja es evidente que si el servidor
                        deja de funcionar o presenta cualquier falla, toda la infraestructura conectada a dichos servidor dejará de funcionar.
                    '/>

                    <Question question='¿Qué ventajas tiene el uso de una base de datos no relacional?'></Question>
                    <AnswerButton answer='Una de las principales ventajas de una base de datos no relacional es su flexibilidad en la creación
                        de los esquemas de información. Además, puede soporter un mayor volumen de datos sin requerir de un servidor adicional, 
                        por último se destaca que son muy funcionales, ya que cuentan con API exclusivas y proporcionan modelos de datos para trabajar
                         con cada tipo de datos presentes en la base.'/>

                    </ul>
                </div>
            </main>
             </Fragment>

    )
}