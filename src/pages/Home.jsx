import React from "react";
import { MyDetails } from "../components/MyDetails";
import { MyAvatar } from "../components/MyAvatar";
import { ShowButton } from "../components/ShowButton";


export const Home = () =>{
    return (
        <div>
            <header>
                    <ShowButton />
                     <MyAvatar />
                        <h2 className= 'Career'>Desarrollador Junior</h2>
                </header>
                <main>
                    <MyDetails />
                </main>
             </div>

    )
}