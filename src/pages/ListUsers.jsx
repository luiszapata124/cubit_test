import React, {useState, useEffect, Fragment} from "react";
import { UserCard } from "../components/UserCard";





export const ListUsers = () =>{


    const [users, setUsers] = useState([])

    const getUsers = async()=>{

        const resp = await fetch("https://reqres.in/api/users?page=1")
        const json = await resp.json()
        setUsers(json.data)
    
    }

    useEffect (() => {
        getUsers()
    },[]
    )

    return <Fragment>
        <div className="row" style={{justifyContent: "center", marginTop: "50px"}}></div>
            <div><h1 className= 'Title'>Listado de usuarios</h1></div>


            <ul className='users-grid'>
                {users.map((user) => {

                       return <li style={{listStyle:'none'}}>

                            <UserCard 
                        titulo={user.first_name}
                        email={user.email}
                        imagen={user.avatar}
                        link={`/users/${user.id}`}
                             />
                           </li>
                                })
                }
                </ul>
                
                
                
    </Fragment>
}