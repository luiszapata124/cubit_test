import React, {useState, useEffect, Fragment} from "react";
import { useParams } from "react-router";
import { DetailsCard } from "../components/DetailsCard";


export const Users = () =>{

    const [user, setUser] = useState([])
    const {id} = useParams()

    const getUser = async()=>{

        const resp = await fetch(`https://reqres.in/api/users/${id}`)
        const json = await resp.json()
        setUser(json.data)
    
    }

    useEffect (() => {
        getUser()
        
    },[]
    )


    

    return (
        <Fragment>
           <header>
               <h1 className="Title">{`${user.first_name} ${user.last_name}`}</h1>
           </header>
           <main>
               <div style={{display:"flex", justifyContent: "center", alignItems: "center"}}>
                <DetailsCard
                    primerNombre={user.first_name}
                    apellido= {user.last_name}
                    email={user.email}
                    imagen={user.avatar}
                 />
               </div>
           </main>
             </Fragment>

    )
}