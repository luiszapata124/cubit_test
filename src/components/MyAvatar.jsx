import { Fragment } from "react";

export function MyAvatar() {
    return (
        <Fragment>
            <img src="https://res.cloudinary.com/dtwiz1r1l/image/upload/v1633972269/batman_eyuo01.png" 
            alt="" 
            width='120'
            height='120'
            style= {{marginBottom: 20, textAlign: "center"}}
            />
        </Fragment>
    )
}
