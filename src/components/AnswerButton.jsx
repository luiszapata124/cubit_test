import React, { Fragment, useState } from 'react';

export const AnswerButton = (props) => {

  const {answer} = props;
  const [showAnswer, setShowAnswer] = useState(false);
  const Ocultar = () => {
    setShowAnswer(!showAnswer);
  }
  
  return (
    <Fragment>
      <button type="button" onClick={Ocultar} className = "answerButton">
        {showAnswer ? 'Ocultar respuesta' : 'Mostrar respuesta'}
      </button>

      {showAnswer ? (<div><h1 className= 'answer'>{answer}</h1></div>) 
      : <div style={{
        marginTop: 20,
        textAlign: 'center'
      }}></div>}
    </Fragment>
  );
};


