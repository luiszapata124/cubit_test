import { Fragment } from "react";

export const Question = (props) =>{

    const {question} = props;

    return (
        <Fragment>
            <li style={{listStyle:'none'}}>
            <h3 className='question'>{question}</h3>
            </li>
        </Fragment>

    )



}