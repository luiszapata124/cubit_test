import React, { Fragment, useState } from 'react';

export const ShowButton = () => {
  const [show, setShow] = useState(false);
  const Ocultar = () => {
    setShow(!show);
  }
  
  return (
    <Fragment>
      <button type="button" onClick={Ocultar} className = "showButton">
        {show ? 'Ocultar identidad' : 'Descubrir identidad'}
      </button>

      {show ? (<div><h1 className= 'Title'>Luis Zapata</h1></div>) 
      : <div style={{
        marginTop: 20,
        textAlign: 'center'
      }}></div>}
    </Fragment>
  );
};


