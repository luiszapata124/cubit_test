
export const DetailsCard = (props) =>{

    const {primerNombre, apellido, email} = props;

    return (
       <div className= "card" style={{width: "25rem", borderRadius: "20px", 
       borderColor:'goldenrod', border:'3px solid',}}>
           
               <div className="card-body">
                   <p className='card-title' style={{marginBottom:'35px'}}>{primerNombre} details:</p>
                   <p className = "detail-card-text">First name: {primerNombre}</p>
                   <p className = "detail-card-text">Last name: {apellido}</p>
                   <p className="detail-card-text">Email: {email}</p>
                   
                   
               </div>
       </div> 
    )

}