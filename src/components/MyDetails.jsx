import { Fragment } from "react/cjs/react.production.min";

export function MyDetails() {
   return (
   <Fragment>
   <p className = 'Details'>A mis 25 años trabajo como Desarrollador Fullstack Junior de Apps en Flutter.<br />
                Además soy estudiante activo de Ingeniería de Sistemas en la Universidad de Antioquia.<br />
              A corto plazo deseo continuar aprendiendo y desarrollando mis habilidades en todo lo relacionado a mi carrera. <br />
               A largo plazo quisiera desempeñarme como el líder de un equipo de desarrollo, <br /> 
               además deseo adquirir conocimientos avanzados en Machine Learning y Data Science. 
                </p></Fragment>);
}