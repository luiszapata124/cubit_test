
import { Fragment } from "react";
import { Link } from "react-router-dom";

export const UserCard = (props) =>{

    const {imagen, titulo, email, link} = props;

    return (
        <Fragment>
       <div className= "card" style={{width: "18rem", borderRadius: "20px", 
       borderColor:'goldenrod', border:'3px solid'}}>
           <img width={260} height={260} style={{borderRadius: "30px", borderColor: "white", padding:"5px 10px 2px 10px"}} src={imagen} className="card-img-top img-thumbnail" alt="..."/>
               <div className="card-body">
                   <h5 className = "card-title">{titulo}</h5>
                   <p className="card-text">{email}</p>
                   <Link to={link} style={{color: 'white'}}>
                   <button href={link} type="button" className = "cardButton">
                    Detalles
                    </button>
                            </Link>
                   
                   
                   
               </div>
       </div> 
       </Fragment>
    )

}